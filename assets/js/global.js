//classes
var name;
function ChatPool(){    
    
    socket.on('user-list', function(event){updateUserList(event);});
    socket.on('new-chat', function(event){updateChatWindow(event);});
    socket.on('user-left', function(event){sayGoodbye(event);});
    
    //Helper to initially setup list
    this.generateUserWindow = function(){
        socket.get('/chatlog/getAllUsers', function(userArray, response){
            updateUserList(userArray);
        });
    }
    
    this.startHandshake = function(){
//        var thisun = this;
        socket.get('/chatlog?name=' + name, function(body, resp){
            switch(body.response){
                case "error":
                    clearChatPane();
                    flagError('Sorry, there has been an error - please try again later.');
                    break;
                case "ok":
                    console.log("ok");
                    setupChatScreen();
                    break;
                case "name taken":
                    clearChatPane();
                    flagError('Sorry, please try a different name.');
                    break;
            }
        });
    }
    
    this.sendMessage = function(aMessage){
        socket.get('/chatlog/sendMessage?message=' + aMessage, function(body, resp){
            switch(body.response){
                case "error":
                    clearChatPane();
                    flagError('Sorry, there has been an error - please try again later.');
                    break;
                case "ok":
                    console.log("ok");
                    break;
            }
        });
    }
    
    this.startHandshake();
        
    
}


$(document).ready(function(){
    var chatter;   
    
    bindNameClick();    
    
});

function setLoading(){
    $('#chat').html("<img src='images/loading.gif' />");
}

function setupChatScreen(){
    $('#chat').html("<div id='chatWindow'></div><input type='text' id='messageBar' size='60'/> <input type='submit' value='send' id='sendMessage' />");
    bindChatWindow();
}

function clearChatPane(){
    $('#chat').html('<p>Please enter a handle (1-10 characters long)</p><input type="text" id="name" /><input type="button" id="chataway" value="Start Chatting">');
    bindNameClick();
}

function flagError(error){
    $('#chat').children('p').remove();
    $('#chat').append("<p class='error'>"+error+"</p>");
}

function bindNameClick(){
    $('#chataway').on('click', function(){
        name = $('#name').val();

        if(name == ""){
            flagError('Please enter a name!');
        }
        else{
            setLoading();
            chatter = new ChatPool();
        }

    });
    $('#name').bind('keypress', function(e) {
        var code = e.keyCode || e.which;
        if(code == 13) { //Enter keycode
            $('#chataway').click();
        }
    });
}

function bindChatWindow(){
    $('#sendMessage').on('click', function(){
        chatter.sendMessage($('#messageBar').val());
        $('#messageBar').val("")
    });
    $('#messageBar').bind('keypress', function(e) {
        var code = e.keyCode || e.which;
        if(code == 13) { //Enter keycode
            $('#sendMessage').click();
        }
    });
}

function updateUserList(userArray){
    var theList = $('#userList'); console.log(userArray);
    theList.empty();
    var stringer;
    for(userId in userArray){ 
        stringer = "<li";
        if(name == userArray[userId]){ stringer += " class='active'";}
        stringer += ">" + userArray[userId] + "</li>"
        theList.append(stringer);
    }
}
function updateChatWindow(message){
    $('#chatWindow').append("<p>["+message.timestamp+"]<span class='bold'>" + message.name + ":</span> " + message.message+"</p>");
}

function sayGoodbye(message){
    $('#chatWindow').append("<p class='systemMessage'>["+message.timestamp+"]<span class='bold'>" + message.name + ":</span> left the room </p>");
}