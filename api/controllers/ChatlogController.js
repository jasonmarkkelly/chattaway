/**
 * ChatController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    
    index: function (req, res) {
        var socket = req.socket;
        
        var newUser = {
            sessionID: socket.id,
            currentlyActive: true,
            name: req.param('name')
        };
        User.find({currentlyActive:1, name:newUser.name}).exec(function(error,users){
            if(users.length > 0){
                return res.json({
                    response: "name taken"
                });
            }
            else{
                User.create(newUser).exec(function createCB(err,created){
                    if(!err){
                        sails.models.user.getAllUsers();
                        return res.json({
                            response: "ok"
                        });
                    }
                    else{ 
                        return res.json({
                            response: "error"
                        });
                    }
                });
            }
        });
    },
  
    getAllUsers: function(req, res){
        sails.models.user.getAllUsers();
    },
  
    sendMessage: function(req, res){
        var socket = req.socket;
        
        var newMessage = { chatID: socket.id, message: req.param('message')};
        console.log(newMessage);
        Chatlog.create(newMessage).exec(function createCB(err,created){
            console.log('Created message with name ' + created.name);
            User.findOne({sessionID: socket.id}).exec(function(error, user){
                sails.io.sockets.emit('new-chat', {name: user.name, timestamp: created.createdAt, message: sails.models.user.escapeHtml(created.message)});
            });
        });
    },
    
  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to ChatController)
   */
  _config: {}

  
};
