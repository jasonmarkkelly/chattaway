/**
 * ChatController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    
    index: function (req, res) {
        var socket = req.socket;

        return res.json({
            response: "ok"
        });
    },
  
    getAllUsers: function(req, res){
        var clients = sails.io.sockets.clients();
        var retarr = new Array();
        for(var a in clients){
            retarr.push({name: clients[a].id});
        }
        return res.json(retarr);
    },
  
    sendMessage: function(req, res){
        
        //Create a new user
        var newMessage = { chatID: socket.id, message: req.param('message')};
        console.log(newMessage);
        Chatlog.create(newMessage).exec(function createCB(err,created){
            console.log('Created message with name ' + created.name);
        });
        
        socket.broadcast.to('chatpool').emit("new-chat", {id: 1, time: "now", message: newMessage});
    },
    
  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to ChatController)
   */
  _config: {}

  
};
