/**
 * Chatlog
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    tableName: 'chatlog',
    adapter: 'mysql',
    types: {
        size: function() {
           return true;
        }
    },
    attributes: {
        chatID: {
          type: 'STRING',
          maxLength: 100,
          minLength: 1,
          size: 100
        },
        message: {
          type: 'STRING',
          maxLength: 1024,
          minLength: 1,
          size: 1024
        }
    },


};