/**
 * Chatlog
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    tableName: 'chat_user',
    adapter: 'mysql',
    attributes: {
        sessionID: {
          type: 'STRING',
          maxLength: 100,
          minLength: 1
        },
        name: {
          type: 'STRING',
          maxLength: 100,
          minLength: 1
        },
        currentlyActive: {
          type: 'boolean'
        }
    },
    getAllUsers: function(){
        User.find({currentlyActive: 1}).exec(function(error, users){
            var retArr = new Array();
            for(var user in users){
                retArr.push(users[user].name);
            }
            sails.io.sockets.emit('user-list', retArr);
        });
    },
    escapeHtml: function(text) {
        var map = {
          '&': '&amp;',
          '<': '&lt;',
          '>': '&gt;',
          '"': '&quot;',
          "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }

};