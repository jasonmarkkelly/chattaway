module.exports.adapters = {
            'default': 'disk',
            disk: {
                    module: 'sails-disk'
            },
            'mysql-adapter': {
                    module: 'sails-mysql',
                    host: '127.0.0.1',
                    user: 'root',
                    password: '',
                    database: 'sails'
            }
    };
